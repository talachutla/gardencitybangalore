import React, { Component } from 'react';
import { AppRegistry,Image, Text, View,StyleSheet,ScrollView } from 'react-native';

class ParentGuide extends Component { 
 constructor(props) {
   super(props);
   var hours = new Date().getHours();
   var hello="";
   if(hours < 12){
     hello = "Good Morning";
   }else if(hours < 16){
     hello = "Good Afternoon";
   }else{
     hello = "Good Evening";
   }
   
   this.state = {
                 hi:hello,
                };
 } 
 render() {

   return (
    
     <View style={{
       flex: 1,
       flexDirection: 'column',
     }}>

       <View style={{width: 450, height: 680,backgroundColor: '#82E1E2'}} >
       <Text style={styles.titleText}>{this.state.hi}
Please visit to the below link to learn more about the disorder

https://www.nimh.nih.gov/health/topics/autism-spectrum-disorders-asd/index.shtml?utm_source=rss_readersutm_medium=rssutm_campaign=rss_full

Also please read the content below:


Autism spectrum disorder (ASD) is the name for a group of developmental disorders. ASD includes a wide range, “a spectrum,” of symptoms, skills, and levels of disability.


People with ASD often have these characteristics:

Ongoing social problems that include difficulty communicating and interacting with others
Repetitive behaviors as well as limited interests or activities
Symptoms that typically are recognized in the first two years of life
Symptoms that hurt the individual’s ability to function socially, at school or work, or other areas of life



                    ThANK YOU .........................
       </Text> 
       </View>
       <ScrollView>
       </ScrollView>
     </View>
   );
 }
 componentDidMount(){

 }

};
const styles = StyleSheet.create({
 baseText: {
   fontFamily: 'Cochin',
 },
 titleText: {
   fontFamily: 'cambria',
   fontSize: 20,
   fontWeight: 'bold',
   textAlign: 'center',
   paddingTop:20,
   color:'#E8FAFB',
 },
});
module.exports=ParentGuide;