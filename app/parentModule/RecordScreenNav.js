'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  ToastAndroid,
  TouchableHighlight,
  Navigator
} from 'react-native';




class RecordScreenNav extends Component {
	constructor(props){
		super(props);

        this.state = {         
            showProgress: true
        };

	};

	render () {
    this.restCall();
		 return (
        <View style={styles.container}>
          <Text style={styles.welcome}>
                Recording...
          </Text>
          
      </View> 
    );  
	}

  restCall(){

     fetch('https://facebook.github.io/react-native/movies.json')
     // fetch('http://192.168.0.158:1337/records?recordUuid=1235')
          .then((response) => response.json())
          .then((responseJson) => {
            alert(responseJson.title);
            ToastAndroid.show('Rest Call Succesful!!', ToastAndroid.SHORT); // responseJson.movies;
          })
          .catch((error) => {
            console.error(error);
          });
  }

};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  }
});

module.exports = RecordScreenNav;