'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  ToastAndroid,
  TouchableHighlight,
  Navigator,
  Icon
} from 'react-native';

var RecordScreenNextPage = require('./RecordScreenNav');


class RecordScreen extends Component {

	constructor(props){
		super(props);

    this.state = {
      isRecordPressed: false
    };

	};

	render () {
		var message = 'Welcome to Autistica !!';
    var isRecordPressed = this.state.isRecordPressed;

    if(this.state.isRecordPressed)
    {
      return(
      <RecordScreenNextPage />
      );

    }else{
  		return(

      			<View style={styles.container}>
            
            <View style={{height: 90, backgroundColor: '#\D6A2DC'}}></View>

            <View style={styles.container2}>  
              <View id="imageCon">
              <Image style={styles.logo} source={require('./images/Good-Morning-Smiley.png')} />
              </View>
      				
      	        	<Text style={styles.welcome}>
      	        		{message}
      				</Text>
              <TouchableHighlight 
              	 onPress={this.onRecordPressed.bind(this)}
              	style={styles.button}>
                   <Text style={styles.buttonText}>Record</Text>
              </TouchableHighlight>
              </View>
            </View> 
    		);
    }
	}

   componentDidMount(){
      // var hours = new Date().getHours();
      // if(hours < 12){
      //     document.getElementById('imageCon').innerHTML = '<Image style={styles.logo} source={require('./images/Good-Morning-Smiley.png')} />';
      // }else if(hours < 16){
      //     document.getElementById('imageCon').innerHTML = '<Image style={styles.logo} source={require('./images/Good-Morning-Smiley.png')} />';
      // }else{
      //     document.getElementById('imageCon').innerHTML = '<Image style={styles.logo} source={require('./images/Good-Morning-Smiley.png')} />';
      // }    
  } 

	onRecordPressed(){       
      ToastAndroid.show('Login Succesful!!', ToastAndroid.SHORT); 
      this.setState({isRecordPressed: true});
  }

};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    backgroundColor: '#FCECFC',

  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  logo: {
  	width: 100,
  	height: 100
  },

   button: {
        height: 80,
        width: 80,
        backgroundColor: '#48BBEC',
        borderColor: '#48BBEC',
        alignSelf: 'center',
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 40
    },
    buttonText: {
        color: '#fff',
        fontSize: 24,
        alignSelf: 'center',
    },
  image: {
  	height:128,
    width: 128,
    borderRadius: 64
  },
    container2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FCECFC',
  },

});

module.exports = RecordScreen;