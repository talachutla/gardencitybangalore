import React, { Component } from 'react';
import { AppRegistry,Image, Text, View,StyleSheet } from 'react-native';

class ParentWelcome extends Component { 
  constructor(props) {
    super(props);
    var hours = new Date().getHours();
    var hello="";
    if(hours < 12){
      hello = "Good Morning";
    }else if(hours < 16){
      hello = "Good Afternoon";
    }else{
      hello = "Good Evening";
    }
    
    this.state = {
                  hi:hello,
                 };
  } 
  render() {

    return (
     
      <View style={{
        flex: 1,
        flexDirection: 'column',
      }}>

        <View style={{width: 250, height: 90,backgroundColor: '#82E1E2'}} >
        <Text style={styles.titleText}>{this.state.hi}</Text> 
        </View>
        <View style={{width: 250, height: 90,backgroundColor: '#E2F5F5'}} />
        <View style={{width: 250, height: 350, flexDirection: 'row', backgroundColor: '#E2F5F5', justifyContent: 'space-around',}} >
           <View style={{width: 100, height: 100, backgroundColor: '#82E1E2'}}>
             <Text style={styles.titleText}>View Behaviour</Text>   
           </View>  
          <View style={{width: 100, height: 100, backgroundColor: '#82E1E2'}}>
             <Text style={styles.titleText}>Guide</Text>  
          </View>
        </View>
      </View>
    );
  }

};
const styles = StyleSheet.create({
  baseText: {
    fontFamily: 'Cochin',
  },
  titleText: {
    fontFamily: 'cambria',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingTop:20,
    color:'#E8FAFB',
  },
});
AppRegistry.registerComponent('AwesomeProject', () => ParentWelcome);