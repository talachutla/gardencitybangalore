'use strict';

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  ToastAndroid,
  TouchableHighlight,
  Navigator,
  Icon
} from 'react-native';

var RecordScreenNextPage = require('./RecordScreenNav');
var HappyPage1 = require('./HappyPage1.js');
var SadPage1 = require('./SadPage1.js');

class RecordScreen extends Component {

	constructor(props){
		super(props);

    this.state = {
      isSmileButtonPressed: false,
      isSadButtonPressed: false
    };

	};

	render () {
		var message = 'How are you doing today ?';

    if(this.state.isSmileButtonPressed)
    {
        return(
          <HappyPage1 />
        );

    }else if(this.state.isSadButtonPressed){
        return(
          <SadPage1 />
        );

    }else{
  		return(

      			<View style={styles.container}>            
            <View style={{height: 90, backgroundColor: '#\D6A2DC'}}></View>

            <View style={styles.container2}>  
              <View id="imageCon">
                <Image style={styles.logo} source={require('./images/emoticons/Happy.png')} />
              </View>
      			
      	     <Text style={styles.welcome}>
      	        		{message}
      				</Text>

               <View style={{flex: 1, flexDirection: 'row'}}>  

               <TouchableHighlight 
               	 onPress={this.onSmileIconPressed.bind(this)}
               	style={styles.smile}>
                     <Image
                        style={styles.logo}
                        source={require('./images/emoticons/smile.png')}
                      />
               </TouchableHighlight>

               <TouchableHighlight 
                   onPress={this.onSadIconPressed.bind(this)}
                   style={styles.sad}>
                     <Image
                        style={styles.logo}
                        source={require('./images/emoticons/Sad.png')}
                      />
               </TouchableHighlight>

               </View>
              </View>
            </View> 
    		);
    }
	}

	onSmileIconPressed(){       
      this.setState({isSmileButtonPressed: true});
  }

onSadIconPressed(){       
      this.setState({isSadButtonPressed: true});
  }

};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    backgroundColor: '#FCECFC',

  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    paddingTop: 30
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  logo: {
    marginTop: 20,
  	width: 100,
  	height: 100,
    paddingTop: 10,
  },
  smile: {
    flex: 1,
    justifyContent: 'flex-start',
    marginTop: 20,
    paddingTop: 10,
    marginLeft: 40, 
    alignItems: 'flex-start',
    backgroundColor: '#FCECFC',
  },
    sad: {
    flex: 1,
    justifyContent: 'flex-start',
    marginTop: 20,
    paddingTop: 10,
    marginRight: 40, 
    alignItems: 'flex-end',
    backgroundColor: '#FCECFC',
  },

   button: {
        height: 80,
        width: 80,
        backgroundColor: '#48BBEC',
        borderColor: '#48BBEC',
        alignSelf: 'center',
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 40
    },
    buttonText: {
        color: '#fff',
        fontSize: 24,
        alignSelf: 'center',
    },
  image: {
  	height:128,
    width: 128,
    borderRadius: 64
  },
    container2: {
    flex: 1,
    justifyContent: 'center',
    marginTop: 20,
    paddingTop: 10,
    alignItems: 'center',
    backgroundColor: '#FCECFC',
  },

});

module.exports = RecordScreen;