'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ToastAndroid,
  TouchableHighlight,
  Navigator
} from 'react-native';

var WhatHappenedPage = require('./WhatHappenedPage.js');

export default class SadPage1 extends Component {
	constructor(props){
		super(props);
	  
    this.state = {
      isSpeakButtonPressed: false,
      isFingerIconPressed: false,
    };
    
  };

  render () {
    var message = 'Do you want to share ?';

    if(this.state.isSpeakButtonPressed)
    {
        return(
          <WhatHappenedPage />
        );

    }else if(this.state.isFingerIconPressed){
        return(
          <SadPage1 />
        );

    }else{
     return (
            <View style={styles.container}>            
                <View style={{height: 90, backgroundColor: '#\D6A2DC'}}></View>
              
                 <Text style={styles.welcome}>
                        {message}
                  </Text>

                   <View style={{flex: 1, flexDirection: 'column'}}>                          
                       <TouchableHighlight 
                          onPress={this.onSpeakIconPressed.bind(this)}
                          style={styles.smile}>
                             <Image
                                style={styles.logo}
                                source={require('./images/emoticons/Speak.png')}
                              />
                       </TouchableHighlight>

                      <TouchableHighlight 
                          onPress={this.onFingerIconPressed.bind(this)}
                          style={styles.fingerTouch}>
                             <Image
                                style={styles.logoFinger}
                                source={require('./images/emoticons/FingerTouch.png')}
                              />
                       </TouchableHighlight>

                    </View>   
            </View> 
    );  
   }
  }

  onSpeakIconPressed(){       
      this.setState({isSpeakButtonPressed: true});
  }

  onFingerIconPressed(){       
      ToastAndroid.show('Finger Icon Pressed !!', ToastAndroid.SHORT); 
      this.setState({isFingerIconPressed: true});
  }
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    backgroundColor: '#FCECFC',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    paddingTop: 30
  },
  logo: {
    width: 100,
    height: 100,
  },
    logoFinger: {
    width: 150,
    height: 120,
  },
  smile: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
    backgroundColor: '#FCECFC',
    alignSelf: 'center'
  },
  fingerTouch: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 60,
    backgroundColor: '#FCECFC',
    alignSelf: 'center'
  },

  DairyButtonText: {
    color: '#fff',
    fontSize: 20,
    alignSelf: 'center',
},
diaryButton: {
        height: 60,
        width: 150,
        borderRadius: 10,
        backgroundColor: '#48BBEC',
        borderColor: '#48BBEC',
        alignSelf: 'center',
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'flex-end',
        paddingRight: 10,
        marginLeft: 20, 
    },
  videoButton: {
        height: 60,
        width: 150,
        borderRadius: 10,
        backgroundColor: '#48BBEC',
        borderColor: '#48BBEC',
        alignSelf: 'center',
        marginTop: 10,
        justifyContent: 'center',
        alignItems: 'flex-end',
        paddingLeft: 10,
        marginRight: 20, 
   },

});

module.exports = SadPage1; 