/**
 * FeelingDictionaryController
 *
 * @description :: Server-side logic for managing FeelingDictionaryController
 * @author      :: Santosh Kumar Talachutla
 */

module.exports = {

  get: function (req, res) {
    var params = req.params.all();
    FeelingDictionaryService.get(params, req.profile, function (err, records) {
      if (err)
        return res.notFound(err);
      return res.ok(records);
    });
  },

  create: function (req, res) {
    var params = req.params.all();
    FeelingDictionaryService.create(params, req.profile, function (err, records) {
      if (err)
        return res.notFound(err);
      return res.ok(records);
    });
  },

update: function (req, res) {
    var params = req.params.all();
    FeelingDictionaryService.update(params, req.profile, function (err, records) {
      if (err)
        return res.notFound(err);
      return res.ok(records);
    });
  },

  destroy: function (req, res) {
    var params = req.params.all();
    FeelingDictionaryService.remove(params, req.profile, function (err, records) {
      if (err)
        return res.notFound(err);
      return res.ok(records);
    });
  },

};