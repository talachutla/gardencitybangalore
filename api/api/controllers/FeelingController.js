/**
 * FeelingController
 *
 * @description :: Server-side logic for managing FeelingController
 * @author      :: Santosh Kumar Talachutla
 */

module.exports = {

  get: function (req, res) {
    var params = req.params.all();
    FeelingService.get(params, req.profile, function (err, records) {
      if (err)
        return res.notFound(err);
      return res.ok(records);
    });
  },

  create: function (req, res) {
    var params = req.params.all();
    FeelingService.create(params, req.profile, function (err, records) {
      if (err)
        return res.notFound(err);
      return res.ok(records);
    });
  },

update: function (req, res) {
    var params = req.params.all();
    FeelingService.update(params, req.profile, function (err, records) {
      if (err)
        return res.notFound(err);
      return res.ok(records);
    });
  },

  destroy: function (req, res) {
    var params = req.params.all();
    FeelingService.remove(params, req.profile, function (err, records) {
      if (err)
        return res.notFound(err);
      return res.ok(records);
    });
  },

};