/**
 * MediaController
 *
 * @description :: Server-side logic for managing MediaController
 * @author      :: Santosh Kumar Talachutla
 */

module.exports = {

  get: function (req, res) {
    var params = req.params.all();
    MediaService.get(params, req.profile, function (err, records) {
      if (err)
        return res.notFound(err);
      return res.ok(records);
    });
  },

  create: function (req, res) {
    var params = req.params.all();
    MediaService.create(params, req.profile, function (err, records) {
      if (err)
        return res.notFound(err);
      return res.ok(records);
    });
  },

update: function (req, res) {
    var params = req.params.all();
    MediaService.update(params, req.profile, function (err, records) {
      if (err)
        return res.notFound(err);
      return res.ok(records);
    });
  },

  destroy: function (req, res) {
    var params = req.params.all();
    MediaService.remove(params, req.profile, function (err, records) {
      if (err)
        return res.notFound(err);
      return res.ok(records);
    });
  },

};