/**
 * ConceptController
 *
 * @description :: Server-side logic for managing ConceptController
 * @author      :: Santosh Kumar Talachutla
 */

module.exports = {

  get: function (req, res) {
    var params = req.params.all();
    ConceptService.get(params, req.profile, function (err, records) {
      if (err)
        return res.notFound(err);
      return res.ok(records);
    });
  },

  create: function (req, res) {
    var params = req.params.all();
    ConceptService.create(params, req.profile, function (err, records) {
      if (err)
        return res.notFound(err);
      return res.ok(records);
    });
  },

update: function (req, res) {
    var params = req.params.all();
    ConceptService.update(params, req.profile, function (err, records) {
      if (err)
        return res.notFound(err);
      return res.ok(records);
    });
  },

  destroy: function (req, res) {
    var params = req.params.all();
    ConceptService.remove(params, req.profile, function (err, records) {
      if (err)
        return res.notFound(err);
      return res.ok(records);
    });
  },

};