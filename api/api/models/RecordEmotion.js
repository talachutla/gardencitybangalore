var uuid = require('node-uuid');

module.exports = {

  attributes: {
    assessment: {
      type: 'json',
      required: true
    },
    expression: {
      type: 'json',
      required: true,
      defaultsTo: '[]'
    },
    userUuid: {
      type: 'string',
      required: true,
      index: true
    },
    expressionScore:{
      type: 'json',
      required: true,
      defaultsTo: '[]'
    },
    isSolutionDone: {
      type: 'boolean',
      defaultsTo: 'false',
      required: true
    },
    toJSON: function () {
      var obj = this.toObject();
      delete obj.createdAt;
      return obj;
    }
  },
};