var uuid = require('node-uuid');

module.exports = {
  attributes: {
  name: {
      type: 'string',
      required: true
    },
    phone:{
      type:'integer'
    },
    dob:{
       type: 'datetime',
       required:true
    },
    relations: {
      type: 'json',
      required: true,
      defaultsTo: '[]'
    },
    isTakeCarer: {
      type: 'boolean',
      defaultsTo: 'false',
      required: true
    },
    toJSON: function () {
      var obj = this.toObject();
      delete obj.createdAt;
      return obj;
    }
  },
};