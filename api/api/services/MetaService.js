/**
 * MetaService
 *
 * @module      :: Service
 * @description :: Service For MetaService handling / any deletion / updation / associations 
 * @author      :: Santosh Kumar Talachutla
 */
var ObjectId = require('mongodb').ObjectID;

module.exports = {

  get: function (params, profile, next) {
    var filters = {};
    if(params.uuid){
      this.getById(params, profile, next);
    }else{
      this.getAll(params, profile, next);
    }
  },

  getAll: function (params, profile, next) {
    var filters = {};
    if(params.name){
      filters.name = params.name
    }
    Meta.find(filters, function (err, responses) {
         next(err, responses);
    });
  },
  getById: function (params, profile, next){
      var filters = {};
      filters._id = new ObjectId(params.uuid);
       Meta.findOne(filters, function (err, response) {
         next(err, response);
      })
  },

 create: function (params, profile, next) {
    Meta.create(params, function (err, response) {
      return next(err, response);
    });
  },

   update: function (params, profile, next) {
    var criteria = {
      _id: new ObjectId(params.uuid),
    }
    delete params.uuid;
    Meta.update(criteria, params).exec(function (err, response) {
      return next(err, response);
    });
  },

  remove: function (params, profile, next) {
    var filters = {};
    filters._id = new ObjectId(params.uuid);
    Meta.destroy(filters, function (err, response) {
      next(err, response);
    });
  },

};