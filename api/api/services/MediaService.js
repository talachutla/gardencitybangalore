/**
 * MediaService
 *
 * @module      :: Service
 * @description :: Service For MediaService handling / any deletion / updation / associations 
 * @author      :: Santosh Kumar Talachutla
 */
var ObjectId = require('mongodb').ObjectID;

module.exports = {

  get: function (params, profile, next) {
    var filters = {};
    if(params.uuid){
      this.getById(params, profile, next);
    }else{
      this.getAll(params, profile, next);
    }
  },

  getAll: function (params, profile, next) {
    var filters = {};
     var filters = {};
    if(params.minAge){
      filters.minAge = {'>=' : params.minAge}
    }
    if(params.maxAge){
      filters.maxAge = {'>=' : params.maxAge}
    }
     if(params.gender){
      filters.gender  = params.gender.split(",")
    }
    if(params.meta){
      filters.meta = params.meta.split(",")
    }
    if(params.feelingType){
      filters.feelingType = params.feelingType.split(",");
    }
    Media.find(filters, function (err, responses) {
         next(err, responses);
    });
  },
  getById: function (params, profile, next){
      var filters = {};
      filters._id = new ObjectId(params.uuid);
       Media.findOne(filters, function (err, response) {
         next(err, response);
      })
  },

 create: function (params, profile, next) {
    Media.create(params, function (err, response) {
      return next(err, response);
    });
  },

   update: function (params, profile, next) {
    var criteria = {
      _id: new ObjectId(params.uuid),
    }
    delete params.uuid;
    Media.update(criteria, params).exec(function (err, response) {
      return next(err, response);
    });
  },

  remove: function (params, profile, next) {
    var filters = {};
    filters._id = new ObjectId(params.uuid);
    Media.destroy(filters, function (err, response) {
      next(err, response);
    });
  },

};