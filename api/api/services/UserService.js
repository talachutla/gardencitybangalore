/**
 * UserService
 *
 * @module      :: Service
 * @description :: Service For UserService handling / any deletion / updation / associations 
 * @author      :: Santosh Kumar Talachutla
 */
var ObjectId = require('mongodb').ObjectID;

module.exports = {

  get: function (params, profile, next) {
    var filters = {};
    if(params.uuid){
      this.getById(params, profile, next);
    }else{
      this.getAll(params, profile, next);
    }
  },

  getAll: function (params, profile, next) {
    var filters = {};
    User.find(filters, function (err, responses) {
         next(err, responses);
    });
  },
  getById: function (params, profile, next){
      var filters = {};
      filters._id = new ObjectId(params.uuid);
       User.findOne(filters, function (err, response) {
         next(err, response);
      })
  },

 create: function (params, profile, next) {
    User.create(params, function (err, response) {
      return next(err, response);
    });
  },

   update: function (params, profile, next) {
    var criteria = {
      _id: new ObjectId(params.uuid),
    }
    delete params.uuid;
    User.update(criteria, params).exec(function (err, response) {
      return next(err, response);
    });
  },

  remove: function (params, profile, next) {
    var filters = {};
    filters._id = new ObjectId(params.uuid);
    User.destroy(filters, function (err, response) {
      next(err, response);
    });
  },

};