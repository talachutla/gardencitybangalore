/**
 * RecordEmotionService
 *
 * @module      :: Service
 * @description :: Service For RecordEmotionService handling / any deletion / updation / associations 
 * @author      :: Santosh Kumar Talachutla
 */
var ObjectId = require('mongodb').ObjectID;

module.exports = {

  get: function (params, profile, next) {
    var filters = {};
    if(params.uuid){
      this.getById(params, profile, function(err,responses){
         next(err, responses);
      });
    }else{
      this.getAll(params, profile, function(err,responses){
         next(err, responses);
      });
    }
  },

  getAll: function (params, profile, next) {
    var filters = {};
    RecordEmotion.find(filters, function (err, responses) {
      console.log(" hello "+responses)
         next(err, responses);
    });
  },

  getById: function (params, profile, next){
       var filters = {};
      filters._id = new ObjectId(params.uuid);
       RecordEmotion.findOne(filters, function (err, response) {
         next(err, response);
      });
  },

 create: function (params, profile, next) {
  this.calcScore(params,profile,function(err, resp){
     RecordEmotion.create(params, function (err, response) {
      return next(err, response);
    });
  });
  },

  calcScore:  function (params, profile, next) {
        var tokens  = params.assessment.text.split(" ");
        FeelingDictionary.find({}, function(err, responses){
          params.expression = [];
          params.expressionScore = [];
          _.forEach(responses, function (obj) {
          var temp = _.intersection(obj.dictionary, tokens);
          console.log(temp.length+"  -- "+obj.dictionary.length)
          var score = (temp.length && temp.length >0 ) ? (temp.length*100/obj.dictionary.length) : 0;
          if(score != 0){
              params.expression.push(obj.name);
              params.expressionScore.push(score);
          }
        });
          if(params.expression.length == 0){
            delete params.expression;
            delete params.expressionScore;
          }
        next(null,params);
        })
  },

  remove: function (params, profile, next) {
    var filters = {};
    filters._id = params.uuid;
    RecordEmotion.destroy(filters, function (err, response) {
      next(err, response);
    });
  },

};