/**
 * isAuthorized
 *
 * @module      :: Policy
 * @description :: Access policy for  General Authorization
 * @author      :: Santosh Kumar Talachutla
 */

module.exports = function (req, res, next) {
  var params = req.params.all();
  req.profile = {};
   return next();
};